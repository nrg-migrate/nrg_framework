/**
 * BaseHibernateEntity
 * (C) 2011 Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 *
 * Created on Aug 29, 2011 by Rick Herrick <rick.herrick@wustl.edu>
 */
package org.nrg.framework.orm.hibernate;

import org.nrg.framework.orm.NrgEntity;

/**
 * Represents the interface for NRG entity classes that use Hibernate for persistence.
 */
public interface BaseHibernateEntity extends NrgEntity {
}
